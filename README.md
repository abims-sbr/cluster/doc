# Cluster user documentation

This repository allows to manage the user documentation for the clusters of the IFB NNCR clusters federation.

| NNCR Cluster     | IFB Contact point     | More information                                                        |
| ---------------  | --------------------- | ----------------------------------------------------------------------- |
| IFB Core Cluster | @lecorguille, @julozi | https://ifb-elixirfr.gitlab.io/cluster/doc/                             |
| ABiMS            | @lecorguille          | https://abims.sb-roscoff.fr/resources/cluster                           |


## Comment fonctionne la documentation ?

La documentation du cluster core de l'IFB s'appuie sur des pages au format Markdown. Ces pages sont transformés en contenu HTML à l'aide de l'outil MkDocs.

Chaque modification de la branche master du dépôt entraîne une regénération et publication automatique de la documentation.

## Comment contribuer ?

### Pour modifier une page de documentation

Cliquez sur le petit crayon en haut de la page à modifier (directement depuis le site de la documentation)

Editez le contenu de la page l'interface de GitLab.

Enregistrez vos modifications dans une nouvelle branche et créer immédiatement une MR vers la branche master.

### Pour ajouter une page de documentation

**Créer une nouvelle branche** sur le dépôt :

```bash
git checkout -b <nom-branche>
```

**Créez un fichier** à la racine du projet pour une documentation en anglais.


Enfin **poussez votre nouvelle branche sur le dépôt** et effectuer une MR vers la branche master.

```bash
git push origin <nom-branche>
```

### Tester le rendu des pages
#### Pour les `docs/*`
Il faut exécuter MkDocs en local.

##### Installation
```bash
python3 -m venv ~/.venv_python3
. ~/.venv_python3/bin/activate
pip install mkdocs mkdocs-material mkdocs-macros-plugin
```

##### Build
```bash
bash build.sh ${DOMAIN}
```

##### Usage
Placez-vous dans le dossier racine du dépôt, puis lancez
```bash
mkdocs serve -f mkdocs_${DOMAIN}.yml
```

Le site est à présent consultable sur http://localhost:8000

Si il y a des changements dans les fichiers d'un DOMAIN, il faudra re-builer `bash build.sh ${DOMAIN}`

#### Pour les `tutorials/*`
Il faut exécuter [`claat`](https://github.com/googlecodelabs/tools/tree/master/claat) en local.

##### Installation
Du moins pour MacOSX :P
```bash
conda create -n go go clangxx_osx-64
. activate go
go install github.com/googlecodelabs/tools/claat@latest
echo 'export PATH="/Users/lecorguille/go/bin:$PATH"' >> ~/.bashrc
claat -h
```
##### Usage
Placez-vous dans le dossier racine du dépôt, puis lancez

```bash
for tutorial in tutorials/*.md; do claat export -o site/tutorials ${tutorial}; done
```
Des mini sites seront générés dans `site/tutorials`

## Spécificité par plateforme
### Mécanisme à 2 balles
- Par plateforme:
  - Un `mkdocs_<plateforme>.yml`
    - Gestion du menu à afficher (choix dans les pages supportées)
    - Gestion des variables (voir plus bas)
  - Un dossier `<plateforme>/`
    - Pages trop spécifiques pour être mis en commun
    - Snippets pour adapter des bouts de pages

Le script [`build.sh`](build.sh) prend en entrée un nom de `<plateform>` ou plus justement son nom d'organisaton GitLab.
- Copie certaines pages ou snippets qui sont dans les dossiers dédiés dans le tronc commun.

### Variables
Il est possible grâce à [mkdocs-macros-plugin](https://mkdocs-macros-plugin.readthedocs.io) de gérer un templating dans le Markdown.

1. `mkdocs.yml`
```yaml
extra:
  platform:
    name: ABiMS
```

2. `pages.yml`
```yaml
The platform {{ platform.name }} is [...]
```

### Snippets
Il est possible grâce à [mkdocs-macros-plugin](https://mkdocs-macros-plugin.readthedocs.io) des "snippets" de partager du texte même dans les pages dédiées des plateformes.

1. `abims-sbr/snippets/slurm/slurm_at_partitions.md`
```md
| Partitions | Time out | Max resources / user | Purpose |
| -- | --:| --:| --:|
| **`fast`** | <= 24 hours | cpu=300, mem=1200GB | **Default** - Regular jobs |
| `long` | <= 30 days | cpu=300, mem=1200GB | Long jobs |
```

2. `docs/slurm/slurm_at.md`
```md
[...]
{% include "slurm/slurm_at_partitions.md" %}
[...]
```