
## Hardware

### Compute nodes

{% include "slurm/slurm_at_nodes_cpu.md" %}

{% include "slurm/slurm_at_nodes_gpu.md" %}

### Storage

DELL FluidFS:

- 2.5 PB of project storage
- 7 TB of scratch

## Software layer

The cluster is managed by [Slurm](https://slurm.schedmd.com/ "It's Highly Addictive!") (version 17.05).

Scientific software and tools are available through [Environment Modules](http://modules.sourceforge.net/) and are mainly based on [Conda](https://docs.conda.io/en/latest/) packages or [Singularity](https://www.sylabs.io/docs/) images.

Operating System: [Ubuntu](https://www.ubuntu.com/) (cluster) and sometimes [CentOS](https://www.centos.org/)

Around the cluster management: [Zabbix](https://www.zabbix.com/),  [Netbox](https://github.com/netbox-community/netbox), [VMware ESX](https://www.vmware.com/).

Deployment and configuration are powered by [Ansible](https://www.ansible.com/) and [GitLab](https://gitlab.com/).
![Schema orchestration](_imgs/ifb-nncr-core-cluster-orchestration-Ansible-GitLab-simple.png)
