
### [SLURM] `Illegal instruction (core dumped)` on some nodes

#### Explanation

We have different generations of computing nodes with different brands and generations of CPU.

It can result that some of our CPU have the needed [instruction set architecture](https://en.wikipedia.org/wiki/Instruction_set_architecture) for your software and some not (too old?).

##### Solution

It's possible to select a set of nodes with some `--constraint` based on the nodes `Features`:

- Vendor: `intel` or `amd`
- CPU familly: `broadwell`, `haswell`, `epyc_rome` ...
- CPU instruction set: `avx2`

To get the features available on the node:

```bash
$ sinfo -e --format "%.26N %.4c %.7z %.7m %.20G %f"
                  NODELIST CPUS   S:C:T  MEMORY                 GRES AVAIL_FEATURES
               n[56-57,59]   48  2:24:1  257000               (null) intel,haswell,avx2
  n[70,73,77,83,85,95,113]   96  2:48:1  257000               (null) amd,epyc_rome,avx2
               n[75,96-98]  256 2:128:1  257000               (null) amd,epyc_rome,avx2
               n[76,78-79]   48  4:12:1  257000               (null) amd,opteron_k10
                n[115-118]   48  2:24:1  257000               (null) intel,broadwell,avx2
                       n99   80  4:20:1 1032000               (null) intel,westmere
                      n100  128  4:32:1 2064000               (null) intel,broadwell,avx2
               gpu-node-01   40  2:20:1  128000     gpu:k80:2(S:0-1) intel,broadwell,avx2
```

Thus, you can for example target nodes that allow `avx2` and from `amd`

```bash
#SBATCH --constraint=avx2&amd
```

💡 If you think that some features are needed, don't hesitate to contact us.

---

### [SLURM] `newgrp` and other `Permission non accordée`

The NFS protocol we use to mount project spaces has a limitation on the number of ldap groups it takes into account. It only takes into account the first 16 groups to which the user belongs.

`newgrp` allows you to get around this limitation, by modifying the user's main group, if the user belongs to more than 16 groups.

We're all agree that `newgrp` is a pain!

#### Access to your project folders with `newgrp`

```bash
[cnorris@slurm1 ~]$ cd /shared/projects/facts/
-bash: cd: /shared/projects/facts/: Permission non accordée

[cnorris@slurm1 ~]$ newgrp facts

[cnorris@slurm1 ~]$ cd /shared/projects/facts/
```

#### Run commands with `sg`

`sg` allow to launch command with a specific group right (without having to switch group with `newgrp`)

```bash
[cnorris@slurm1 ~]$ ls /shared/projects/facts/
ls: impossible d ouvrir le répertoire '/shared/projects/facts/': Permission non accordée

[cnorris@slurm1 ~]$ sg facts -c "ls /shared/projects/facts/"
impossible_script.sh       
```

#### TIPS: Recover colours and history after `newgrp`

To recover colours and history, we suggest you run these 2 lines which will add "what might work" in the .bashrc :

```bash
echo "alias newgrp='export NEWGRP=1; newgrp'" >> ~/.bashrc
echo 'if [[ ! -z "$NEWGRP" && -z "$SLURM_JOB_ID" ]]; then unset NEWGRP; bash -l; fi' >> ~/.bashrc
```

**Explanation:**

```bash
alias newgrp='export NEWGRP=1; newgrp'
```

Before our `newgrp`, we'll create a `NEWGRP` variable which will be accessible in the `newgrp` subshell

```bash
if [[ ! -z "$NEWGRP" && -z "$SLURM_JOB_ID" ]]
```

A quick test, to see if the `NEWGRP` variable exists and to check that the `SLURM_JOB_ID` variable does not.

For the latter, we prefer to be cautious. There is a distinction between interactive and non-interactive shells.
`newgrp` launches a non-interactive shell, which means that it does not load `profile.d` and `~/.bash_history`

On the other hand, we would not like all shells to be considered as interactive, particularly in the case of a SLURM job.

```bash
bash -l
```

This launches a bash shell which will be interactive.

On the negative side, it creates a shell (`bash -l`) in a subshell (`newgrp`) in a shell (`ssh slurm1`). So there are 3 exits to go back.

```bash
unset NEWGRP
```

This is to avoid looping (I've tested ^^').

So basically, we empty the `NEWGRP` variable so as not to restart `bash -l` indefinitely at each `bash -l` :)

---

### [SLURM] sqlite3 `Error: disk I/O error`

```bash
$ sqlite3  FooBar_AnvioContigs.db ".tables"
Error: disk I/O error
```
<!-- markdownlint-disable-next-line MD024 -->
#### Explanation

Il peut y avoir des soucis lors de l'usage de sqlite3 à travers les montages NFS (`/shared/projects/...`) à cause de latence d'écriture. Cela dépend parfois du point de montage.

(Note que ce cas se présente aussi si un programme que vous utilisez fait appel à une base sqlite)

<!-- markdownlint-disable-next-line MD024 -->
#### Solution

One solution is to activate the option [WAL for "Write-Ahead Log"](https://www.sqlite.org/wal.html) on the database.

**BUT** to do this, you need to be in a space where the problem does not occur. So, perhaps use the `/tmp` of a node temporarily and if there is space. Some users giva good feedback in using `/shared/scratch` as an option.

Otherwise, contact support for help.

```bash
cp my_database.db /tmp

module load sqlite/3.30.1
sqlite-utils enable-wal /tmp/my_database.db

cp /tmp/my_database.db .
```

---
