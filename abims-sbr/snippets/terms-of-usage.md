
<!-- 
⚠️ The two version dates (fr and en) have to be updated in case the contain is changed 
-->

## 🇫🇷 Charte d'utilisation de l’infrastructure de calcul et de stockage de la plateforme ABiMS

Version: 31/07/2024

La présente charte a pour objet de définir les conditions d’utilisation et les règles de bon usage des ressources informatiques proposées par la plateforme de bioinformatique ABiMS de la Station Biologique de Roscoff.

ABiMS fournit différents services et accès :

- Des ressources de calcul et de stockage,
- Des services incluant la mise à disposition de banques de données et de logiciels, des cycles d'apprentissages / formation, l'hébergement de sites web et de machines virtuelles, une expertise et un support scientifique à des projets relevant de la biologie ou de la bioinformatique.

Plan de la charte :

- Conditions Général à tous les services
- Conditions spécifique à l’usage du Cluster de Calcul

### Conditions Générales à tous les services

#### Accès à l'infrastructure

Pour bénéficier des ressources informatiques d’ABiMS, une demande d’accès doit être effectuée le portail de gestion de compte et de projet : <my.sb-roscoff.fr>. Cette demande est soumise à une validation par l'équipe du site et à l'acceptation de la présente charte. L’autorisation d’accès accordée est strictement personnelle, incessible et temporaire. Elle sera retirée dès lors que la fonction de l’utilisateur ne le justifie plus, si non renouvelée ou en cas de non respect de la présente charte ou de celles du site accédé ou de son établissement hébergeur.

#### Informations et Mot de passe

- Chaque utilisateur s'engage à communiquer au support tout changement ou complément d'informations le concernant (mail, laboratoire...). L’utilisateur est responsable de la mise à jour de son email de contact en cas de changement dû par exemple à un changement de laboratoire.
- Chaque utilisateur s'engage à protéger son compte informatique par un mot de passe, qu'il renouvellera régulièrement (idéalement annuellement).
- L'utilisateur est responsable des dommages qui pourraient provenir d'une mauvaise utilisation de son compte s'il est établi qu'il en a donné le mot de passe.

#### Informations aux utilisateurs

En acceptant cette charte, vous acceptez d’intégrer la liste de diffusion <abims-users@groupes.france-bioinformatique.fr> hébergée par l’Institut Français de Bioinformatique (IFB). La plateforme ABiMS se réserve de communiquer sur cette liste pour les changements de politique, mise en place de nouveaux services, interruptions de service… et pour diffuser son enquête de satisfaction annuelle. L'adresse mail utilisée pour une communication par email sera celle communiquée par l’utilisateur lors de sa demande de création de compte. Les utilisateurs s'engagent à se conformer aux différentes directives communiquées par l’équipe d’administration.

#### Cas des données dites sensibles ou de santé humaine

Notre infrastructure informatique n'étant pas certifiée HDS (Hébergement de Données de Santé), les données de santé collectées dans le cadre d'activités de prévention, de diagnostic et de soins ou liées au suivi social et médico-social ne peuvent être téléchargées, stockées ou traitées (article L.1111-8 du Code de la Santé Publique).

L'utilisateur certifie par la présente qu'il ne téléchargera pas, ne stockera pas et ne traitera pas les données de santé collectées dans le cadre d'activités de prévention, de diagnostic et de soins ou liées au suivi social et médico-social.

#### Règlement général sur la protection des données RGPD/GPRD

Avec l'entrée en vigueur du Règlement Général sur la Protection des Données (RGPD) depuis le 25 mai 2018, nous souhaitons vous informer en toute transparence sur la collecte et le traitement de vos données personnelles réalisé par la plateforme ABiMS. L’ouverture d’un compte, pour accéder au cluster, pour utiliser l’une des instances web ou l’une des bases de données hébergées par la plate-forme, nous amène à collecter les informations personnelles suivantes : nom, prénom, affiliation, adresse mél, horodatages des connexions.

Ces données, ainsi que les celles que vous êtes susceptible de déposer dans l’espace de stockage qui vous est attribué, ne sont en aucune manière rendues publiques ou communiquées à des tiers, à l’exception de “tiers autorisés” :

(<https://www.cnil.fr/sites/default/files/atoms/files/recueil-procedures-tiers-autorises.pdf>).

Les logs de connexion sur nos serveurs sont conservés pour des raisons légales durant un an.

Vous êtes en droit de nous adresser à tout moment une demande de communication, modification ou suppression de vos données personnelles et scientifiques, en utilisant l’adresse mél de contact ({{ platform.support_link }}).

### Conditions spécifiques à l’usage du Cluster de Calcul

L'infrastructure mise à disposition est destinée aux biologistes, bioinformaticiens et bio-statisticiens ayant des besoins de calcul intensif de volumétrie moyenne (de la 100aine de Go au To) et d'un environnement bioinformatique (logiciels, banques de données, support ...). ABiMS se réserve le droit de réorienter certains projets sur les très grands centres de calcul nationaux lorsque la situation le justifiera.

#### Déontologie

- Les responsables des ressources se réservent le droit d’empêcher l’accès à un utilisateur en cas de dépassement de quotas, ou en cas de non-respect des règles énoncées dans la présente charte, ou dans la charte du site accédé ou celle de son organisme hébergeur.
- Dans le cas de travaux confidentiels, il est de la responsabilité de l'utilisateur de garantir leur confidentialité.

Le personnel en charge du site accédé ne peut en aucune façon être considéré comme responsable soit de la perte de ces données, soit de leur diffusion.

#### Engagements d’ABiMS

Chaque compte utilisateur donne droit aux différents services.

1. Un accès au serveur de login
2. L'affectation d'espaces de stockage et quota : l'affectation de l'espace de stockage est subordonnée aux possibilités/capacités. Plusieurs types d’espace sont disponibles : des espaces projets répliqués ou non, sauvegardés ou non et des espaces temporaires (dit scratch) où les données non accédées depuis plus d’un délai fixé sont purgées. Pour disposer de plus d'espace disque, il est nécessaire d‘effectuer une demande exceptionnelle via l’email support {{ platform.support_link }}. Celle-ci sera examinée par la direction, et pourra, selon les cas, impliquer l'établissement d'un devis.
3. L’accès aux banques de données du domaine.
4. Des environnements logiciel bioinformatique.
5. Un support ({{ platform.support_link }}) : dans les limites de la disponibilité des agents en charge du support ;
6. Un accès au calcul.

La plateforme ABiMS s'engage à faire de son mieux pour limiter au maximum les interruptions de service, mais ne peut pas garantir la disponibilité permanente des ressources. ABiMS s'engage à prévenir les utilisateurs des maintenances matérielles et systèmes planifiées impliquant une coupure de service.

#### Vie des comptes et des espaces projets

- Les comptes utilisateurs doivent être étendus explicitement par l’utilisateur tous les 12 mois. L’utilisateur reçoit un email l’invitant à effectuer ce renouvellement.
- Les espaces projets doivent être étendus explicitement par le porteur tous les 12 mois. Le porteur reçoit un email l’invitant à effectuer ce renouvellement.

Pour les comptes et les espaces projets, l’effacement des données pourra être déclenché par la plateforme après 6 mois sans action d'extension ou de renouvellement des utilisateurs.

#### Installation de logiciels ou banques de données génomiques

L’installation et l’utilisation de logiciels ou de banques par les utilisateurs doit se faire dans le respect des dispositions du code de la propriété intellectuelle. En particulier, l’utilisateur doit être en mesure de présenter une licence en règle en cas de demande. L’installation de logiciels ou utilitaires pouvant porter atteinte à l’intégrité des systèmes n’est pas autorisée.

Une demande d'installation multi-users de logiciels ou de banques peut être faite au support ({{ platform.support_link }}). L'installation sera réalisée par l'équipe d'administration système de la plate-forme dans la limite des possibilités : pertinence, temps, ressources nécessaires, dépendances logicielles et licence.

#### Conditions d'utilisation de l'infrastructure

##### Soumission des programmes de calcul

Les ressources de calcul sont accessibles grâce à un  gestionnaire de tâches ou ordonnanceur :  SLURM. Les programmes des utilisateurs doivent être soumis en utilisant le gestionnaire de tâches. Le contournement de cet ordonnanceur et de ses règles de priorité peut donner lieu à une suspension immédiate du programme soumis et à une éventuelle suppression de l'accès à l'infrastructure.

L'utilisateur est responsable des tâches qu'il lance tant sur les serveurs de login que sur les ressources de calcul. Il doit les suivre et les supprimer en cas de problème.

Les serveurs de login sont réservés exclusivement à la connexion, au transfert de données, au test de la ligne de commande et à la soumission de jobs sur le cluster de calcul. Tout traitement de données lancé directement sur les serveurs de login sera systématiquement interrompu sans préavis par les administrateurs système.

##### Stockage des données

Chaque utilisateur est tenu de n'utiliser que la quantité d'espace disque qui lui est strictement nécessaire, et d'utiliser efficacement les moyens de compression et d'archivage. Les fichiers temporaires de calcul doivent être supprimés à l’issue du calcul.

Il est de la responsabilité de l’utilisateur de gérer ses données (organisation, volumétrie, pertinence, ancienneté). La politique de sauvegarde impose des règles d’organisation de dossier. Le plateforme ABiMS n’assure pas de service d’archivage des données.
En complément d’information sur les usages et les politiques de sauvegarde, veuillez vous référer régulièrement à notre documentation dédiée : <https://{{ platform.git_repo_root }}.gitlab.io/cluster/doc/data/data/>

##### Respect du caractère confidentiel des informations

Lors de leur création, les répertoires des utilisateurs ont des droits par défaut conformément aux pratiques du site accédé. Il appartient à l'utilisateur de gérer ses droits comme il le souhaite en changeant les droits par défaut (commande `chmod`) . Les fichiers de chacun sont privés.

Toute tentative de lecture ou de copie de fichiers d’un autre utilisateur sans son autorisation, de même que l’interception de communications entre utilisateurs est répréhensible et pourra, entre autres, entraîner la suppression de l’accès à l’infrastructure.

##### Propriété et valorisation des résultats

L'utilisation des ressources de la plateforme ABiMS ne modifie en rien la propriété ou les conditions d'utilisation des données qui y sont stockées ni celles des résultats obtenus. Toutefois, en cas de valorisation de ces derniers, il est demandé d'inclure la formulation suivante aux communications ou publications :

> *We are grateful to  the Roscoff Bioinformatics platform ABiMS (<http://abims.sb-roscoff.fr>), part of the Institut Français de Bioinformatique (ANR-11-INBS-0013 and  ANR-21-ESRE-0048) and BioGenouest network, for providing help and/or computing and/or storage resources.*

Dans le cadre d’une collaboration ne faisant pas l’objet de prestations, l’équipe de recherche s’engage à valoriser les développements et les analyses en associant à d’éventuelles publications les ingénieurs de la plateforme.
Les codes et les workflows d’analyse développés lors de ces collaborations seront publiés en open-source dans une optique de Science Ouverte sauf directive contraire du demandeur.

## 🇬🇧 Terms of Use Agreement for the use of the computing and storage infrastructure of the ABiMS platform

Version: 31/07/2024

The purpose of this charter is to define the conditions of use and the rules for the proper use of the computer resources offered by the ABiMS bioinformatics platform of the Station Biologique de Roscoff.

ABiMS provides various services and access:

- Computing and storage resources,
- Services including the provision of databases and software, learning/training cycles, hosting of websites and virtual machines, expertise and scientific support for projects in biology or bioinformatics.

Outlines of the charter:

- General conditions for all services
- Specific conditions for the use of the Computing Cluster

### General conditions for all services

#### Access to the infrastructure

To benefit from ABiMS computer resources, a request for access must be made through the account and project management portal: my.sb-roscoff.fr. This request is subject to validation by the site team and acceptance of this charter. The access authorization granted is strictly personal, non-transferable and temporary. It will be withdrawn as soon as the user's function no longer justifies it, if it is not renewed or in the event of non-compliance with this charter or those of the site accessed or its host institution.

#### Information and password

- Each user undertakes to communicate to the support team any change or additional information concerning him/her (e-mail, laboratory, etc.). The user is responsible for updating his contact email in case of change due to a change of laboratory for example.
- Each user undertakes to protect his/her computer account with a password, which should be renewed regularly (ideally annually).
- The user is responsible for any damage that may result from the misuse of his/her account if it is established that he/she has given the password.

#### Information for users

By accepting this charter, you agree to join the <abims-users@groupes.france-bioinformatique.fr> mailing list hosted by the French Institute of Bioinformatics (IFB). The ABiMS platform reserves the right to communicate on this email for policy changes, implementation of new services, service interruptions... and to distribute its annual satisfaction survey. The email address used for communication by email will be the one communicated by the user when requesting the creation of an account. Users agree to comply with the various directives communicated by the administration team.

#### Sensitive or human health data

As our computing infrastructure is not HDS-certified (Health Data Hosting), health data collected during activities of prevention, diagnostic and care or related to social and medical and social follow-up cannot be uploaded, stored or processed (article L.1111-8, French Public Health Code).

The user hereby certify that he will not upload, store or process health data collected during activities of prevention, diagnostic and care or related to social and medical and social follow-up.

#### General Data Protection Regulation RGPD/GPRD

Under the General Data Protection Regulation (GDPR) since 25 May 2018, we wish to inform you in full transparency about the collection and processing of your personal data carried out by the ABiMS platform. The opening of an account, to access the cluster, to use one of the web instances or one of the databases hosted by the platform, leads us to collect the following personal information: surname, first name, affiliation, e-mail address, connection timestamps, etc.

This data, as well as the data that you are likely to deposit in the storage space allocated to you, is in no way made public or communicated to third parties, with the exception of "authorised third parties":

(<https://www.cnil.fr/sites/default/files/atoms/files/recueil-procedures-tiers-autorises.pdf>).

The connection logs on our servers are kept for legal reasons for one year.
You are entitled to send us at any time a request for communication, modification or deletion of your personal and scientific data, using the contact e-mail address ({{ platform.support_link }}).

### Specific conditions for the use of the Computing Cluster

The infrastructure made available is intended for biologists, bioinformaticians and biostatisticians with medium-volume intensive computing needs (from 100 GB to TB) and a bioinformatic environment (software, databases, support, etc.). ABiMS reserves the right to refer certain projects to the very large national computing centres when the situation warrants it.

#### Deontology

Those in charge of the resources reserve the right to prevent access to a user in the event that quotas are exceeded, or in the event of non-compliance with the rules set out in this charter, or in the charter of the site accessed or that of its hosting organisation.

In the case of confidential work, it is the responsibility of the user to guarantee its confidentiality.

The staff of the accessed site can in no way be considered responsible for either the loss of this data or its distribution.

#### Commitments of ABiMS

Each user account gives the right to the different services.

1. Access to the login server
2. Allocation of storage space and quota: the allocation of storage space is subject to the possibilities/capacities. Several types of space are available: replicated or non-replicated project space, backed up or not, and temporary space (known as scratch space) where data that has not been accessed for more than a set period of time is purged. In order to have more disk space, it is necessary to make an exceptional request via the support email {{ platform.support_link }}. This will be examined by the management and may, depending on the case, involve the drawing up of an estimate.
3. Access to the domain's databases.
4. Bioinformatics software environments.
5. Support ({{ platform.support_link }}): within the limits of the availability of the agents in charge of support;
6. Access to computing: the ABiMS platform undertakes to do its best to limit service interruptions as much as possible, but cannot guarantee the permanent availability of resources. ABiMS undertakes to inform users of planned hardware and system maintenance involving a break in service.

#### Life of accounts and project spaces

- User accounts must be explicitly extended by the user every 12 months. The user receives an email inviting him/her to renew.
- Project spaces must be explicitly extended by the holder every 12 months. The holder receives an email inviting him/her to renew.

For accounts and project spaces, the deletion of data may be triggered by the platform after 6 months without any news from the users.

#### Installation of software or genomic databases

The installation and use of software or databases by users must comply with the provisions of the intellectual property code. In particular, the user must be able to present a valid licence in case of request. The installation of software or utilities that could damage the integrity of the systems is not permitted.

A request for multi-user installation of software or banks can be made to the support ({{ platform.support_link }}). The installation will be carried out by the platform's system administration team within the limits of the possibilities: relevance, time, necessary resources, software dependencies and licenses.

#### Conditions of use of the infrastructure

##### Submission of computing programs

Computing resources have a task manager or scheduler: SLURM. User programs must be submitted using the task manager. Bypassing this scheduler and its priority rules may result in immediate suspension of the submitted program and possible removal of access to the infrastructure.

The user is responsible for the tasks he/she launches on both the login server and the computing resources. He/she must monitor them and delete them in the event of a problem.

The login servers are reserved exclusively for logging in, transferring data, testing the command line and submitting jobs on the computing cluster. Any data processing started directly on the login servers will be systematically interrupted without prior notice by the system administrators.

##### Data storage

Each user is required to use only as much disk space as is strictly necessary, and to make efficient use of compression and archiving facilities. Temporary calculation files must be deleted at the end of the calculation.

It is the user's responsibility to manage his data (organisation, volume, relevance, age). The backup policy imposes file organisation rules. The ABiMS platform does not provide a data archiving service.

For more information on the uses and backup policies, please refer to our documentation dedicated to this subject: <https://{{ platform.git_repo_root }}.gitlab.io/cluster/doc/data/data/>

##### Respecting the confidentiality of information

When they are created, user directories have default rights in accordance with the practices of the accessed site. It is up to the user to manage their rights as they wish by changing the default rights (command `chmod`). Everyone's files are private.

Any attempt to read or copy another user's files without their permission, as well as intercepting communications between users, is reprehensible and may, among other things, result in the removal of access to the infrastructure.

##### Ownership and valorisation of results

The use of the resources of the ABiMS platform does not modify the ownership or the conditions of use of the data stored there nor of the results obtained. However, in case of valorisation of the latter, it is requested to include the following wording in the communications or publications:

> *We are grateful to the Roscoff Bioinformatics platform ABiMS (<http://abims.sb-roscoff.fr>), part of the Institut Français de Bioinformatique (ANR-11-INBS-0013 and  ANR-21-ESRE-0048) and BioGenouest network, for providing help and/or computing and/or storage resources.*

In the context of a collaboration that is not subject to services, the research team undertakes to enhance the value of the developments and analyses by associating the platform's engineers with any publications.

The codes and analysis workflows developed during these collaborations will be published in open-source format in an Open Science perspective, unless otherwise specified by the applicant.
