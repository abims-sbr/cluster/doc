
The ABiMS Cluster hosted at the Station Biologique de Roscoff is part of the National Network of Compute Resources (NNCR) of the Institut Français de Bioinformatique (IFB).

<a href="http://abims.sb-roscoff.fr.fr/">
<img src="./_imgs/PF/abims-blue-orange-medium.png" alt="ABiMS logo" style="width:500px; margin-left: 60px;"/>
</a>

| Current available services |  |
|--:|---|
| [{{ account_manager.url }}]({{ account_manager.url }})  | Cluster Account Manager |
| {{ cluster.url }}  | High Performance Computing  |
| [{{ rstudio.url }}]({{ rstudio.url }})  | RStudio server in your browser |
| {{ platform.support_link }}  | {{ platform.name }} Cluster support |

| Services in beta |  |
|--:|---|
| [{{ ondemand.url }}]({{ ondemand.url }})  | RStudio server in your browser with Open OnDemand |
| [{{ ondemand.url }}]({{ ondemand.url }})  | Jupyter Interactive Notebook with Open OnDemand |

| And even more |  |
|--:|---|
| [https://**community**.france-bioinformatique.fr](https://community.france-bioinformatique.fr) | IFB Mutual help and community support |
| [{{ platform.gitlab }}]({{ platform.gitlab }}) | Contribute ! |

## Hosted by

<a href="https://wwww.sb-roscoff.fr">
<img src="./_imgs/PF/sbr_couleur.png" alt="ABiMS logo" style="width:250px; margin-left: 60px;"/></a>

## Funding

This infrastructure has been financed by multiple sources :

- Région Bretagne - Biogenouest
- Conseil Général - CG29
- ANR projects
- Station Biologique de Roscoff: FR2424, UMR7139, UMR7144
- Investissements d'Avenir projects: IFB, EMBRC-France, IDEALG, Océanomics
- GRSIBI projects
