
| Partitions | Time out | Max resources / user | Purpose |
| -- | --:| --:| --:|
| **`fast`** | <= 24 hours | cpu=300, mem=1500GB | **Default** - Regular jobs |
| `long` | <= 30 days | cpu=300, mem=1500GB | Long jobs |
| `bigmem` | <= 60 days | mem=2500GB | On demand - For jobs requiring a lot of RAM |
| `clc` | <= 30 days | cpu=300, mem=1500GB | On demand - Access CLC Assembly Cell |
| `gpu` | <= 10 days | cpu=300, mem=1500GB | On demand - Access GPU cards |

### The default values

| Param | Default value |
| -- | --:|
| `--mem` | 2GB |
| `--cpus` | 1 |
