
(Last update: 19/09/2024)

### GPU nodes

| Nbr | GPU | CPU | RAM (GB )| Type | Disk /tmp | Disk /bank |
| --:| --:| --:| --:| --:| --:| --:|
| 2 | 2 | 80 | 512 | [NVIDIA l40s](https://www.nvidia.com/fr-fr/data-center/l40s/) | ~64 GB | 3.5 TB |
| 1 | 2 | 40 | 128 | [NVIDIA k80](https://www.nvidia.com/fr-fr/data-center/tesla-k80/) | 32 GB | - |

#### GPU Instance Profiles

⚠️ The values below can change. To check the current situation:

```bash
sinfo -Ne -p gpu --format "%.15N %.4c %.7m %G"
```

| Profile Name | GPU Memory  | Number of Instances Available |
| --:| --:| --:|
| l40s | 24GB | 4 |
| k80 | 48GB | 2 |
