---
title: The concept of project at IFB
---

## General issues

To ensure good data lifecycle management with our users, we strive to identify the projects that are processed on the cluster.

We want to avoid large unfinishable project folders with a mess inside (sub-sub-projects).

A project has a perimeter: a subject, a goal, a beginning, an end... A typical project should take place in a window of 1 to 3 years. We can consider the project as a funding grant (obtained or not :/).

Note that if you manage several projects in parallel, it is quite possible for you to request additional project spaces with their own lifetime and list of contributors.

![Data lifecycle schema](../_imgs/data/Data_lifecycle.png)

([source](https://en.wikipedia.org/wiki/Data_management))

FYI, in the future, we plan to request a DMP for each project request.

## A good project name

The ideal name for a project is its ANR acronyme. But you can compose with:

- the species
- the response being studied
- the technologies or methods involved

The ideal name isn't too long :)

Avoid:

- your name
- the name of your laboratory or team
- anything not relevant to your study

## Slurm accounts and projects

Each project hosted on the cluster is associated with a SLURM account. The SLURM accounts match the project names.

NOTE that to submit jobs through SLURM you need at least one SLURM account, either the "demo" account or your own account.

Your user name is by default associated with the "demo" SLURM account which is limited to 6 computing hours.
You need to request the creation of your own project to have more computing hours.

All your SLURM jobs will be accounted on your default account unless you specify a different account.

The `-A or --account` option in srun or sbatch lets you specify which account you want to use.

```bash
srun -A myproject1 hostname
sbatch -A myproject2 awesome_script.sh
```

To view which is your default account using the sacctmgr command:

```bash
sacctmgr show user <your-login> format=User,DefaultAccount
```

You can change your default account using the sacctmgr command :

```bash
sacctmgr update user <your-login> set defaultaccount=<project-name>
```
