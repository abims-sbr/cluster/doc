---
title: Troubleshooting
navigation_depth: 2
---

💡 The following Troubleshooting can be completed by consulting the [IFB Community Forum](https://community.france-bioinformatique.fr/)

---

### [SLURM] `Invalid account or account/partition combination specified`

Complete message:

```bash
srun: error: Unable to allocate resources: Invalid account or account/partition combination specified
```

#### Explanation 1

Your current default SLURM account should be the **`demo`** one. You may have seen a red notice at login? You can check that using:

```bash
$ sacctmgr list user $USER
      User   Def Acct     Admin
---------- ---------- ---------
   cnorris       demo      None
```

##### Solution

If you don't already have a project, you have to request one from the platform: [{{ account_manager.url_project }}]({{ account_manager.url_project }})

Otherwise, you already have a project/account, you can either:

- Specify at each job your SLURM account:

```bash
srun -A my_account command
```

```bash
#!/bin/bash
#SBATCH -A my_account
command
```

- Change your default account

```bash
sacctmgr update user $USER set defaultaccount=my_account
```

{% include "how-to-access-a-terminal.md" %}

⚠️ status_bar is updated hourly. So it may still display demo as your default account by don't worry, it should have work.

---

### [RStudio] Timeout or do not start

Try to clean session files and cache:

```bash
# Remove (rm) or move (mv) RStudio files
# mv ~/.rstudio ~/.rstudio.backup-2022-27-02
rm -rf ~/.rstudio
rm -rf ~/.local/share/rstudio
rm .RData
```

Retry.

If it doesn't work, try to remove your configuration (settings will be lost)

```bash
rm -rf ~/.config/rstudio
```

{% include "how-to-access-a-terminal.md" %}

Retry.

If it doesn't work, contact the support ([IFB Community Forum](https://community.france-bioinformatique.fr/))

---

### [JupyterHUB] Timeout or do not start

Kill your job/session using the web interface (Menu "File" --> "Hub Control Panel" --> "Stop server") or in command line:

```bash
# Remove running jupyter job
scancel -u $USER -n jupyter
```

Clean session files, cache:

```bash
# Remove (rm) or move (mv) JupyterHUB directories
# mv ~/.jupyter ~/.jupyter.backup-2022-27-02
rm -rf ~/.jupyter 
rm -rf ~/.local/share/jupyter
```

{% include "how-to-access-a-terminal.md" %}

---

<!-- markdownlint-disable-next-line MD052 -->
### [SLURM][RStudio] `/tmp No space left on device` / `Error: Fatal error: cannot create 'R_TempDir'`
<!-- markdownlint-disable-next-line MD024 -->
#### Explanation 1

The server on which the job ran must have a full on its `/tmp/`. Indeed, by default, R by default, is writing temporary files in the `/tmp/`  directory of the server.

The local directory `/tmp/` is limited and shared. It's not a good practice to let a software writing on local disk.
<!-- markdownlint-disable-next-line MD024 -->
##### Solution

The solution is to change the default temporary directory and expect that the tool is well developed (and the /tmp not hard-coded).

Please add the following lines at the beginning of your `sbatch` script.

```bash
#!/bin/bash
# SBATCH -p fast

TMPDIR="/shared/projects/my_intereting_project/tmp/"
TMP="${TMPDIR}"
TEMP="${TMPDIR}"
mkdir -p "${TMPDIR}"
export TMPDIR TMP TEMP

module load r/4.1.1
Rscript my_script.R
```

---

{% include "troubleshooting_pf.md" %}
