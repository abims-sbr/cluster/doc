---
title: Starting a JupyterLab session
---

The JupyterLab environment lets you create and run Python, R and Bash notebooks and access a Unix terminal.

You can also install your own JupyterLab kernel with custom libraries.

JupyterLab is the ideal environment for interactive data analysis, including machine learning.

Select the "JupyterLab" choice from the Interactive apps menu.

You can choose the cluster resources you need for your JupyterLab session. Such as project account, partition, number of CPUs, amount of memory.

![Setup_JupyterLab_session](../../_imgs/software/openondemand/Setup_JupyterLab_session.png)

Click Launch to submit the JupyterLab job to the queue. The wait time depends on the current load of the cluster. Requesting smaller, shorter jobs may facilitate shorter wait times.

![JupyterLab_starting](../../_imgs/software/openondemand/JupyterLab_starting.png)

When JupyterLab is ready (running) click on "**Connect to Jupyter**". Your JupyterLab session will be opened in a new tab.

![JupyterLab_running](../../_imgs/software/openondemand/JupyterLab_running.png)

The JupyterLab launcher offers a collection of notebook editors, consoles and application like a Unix terminal, a diagram or text editor.

!!! tip

    When you choose to create a new notebook by click a notebook icon, **make sure that the file navigation panel located at the left is pointing to a directory where you have write access**. Otherwise, JupyterLab will trigger an error message telling that you can't create a new notebook file.

![JupyterLab](../../_imgs/software/openondemand/JupyterLab.png)
