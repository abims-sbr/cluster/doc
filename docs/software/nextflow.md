---
title: Nextflow
---

This doc need ❤️

## Using a "regular" NextFlow workfow

{% include "software/nextflow.md" %}

### Functional annotation with Orson pipeline

Orson is a functional annotation pipeline developed in Nextflow by the [SeBiMER](https://ifremer-bioinformatics.github.io/)team and available at this address : [https://gitlab.ifremer.fr/bioinfo/workflows/orson/-/tree/master](https://gitlab.ifremer.fr/bioinfo/workflows/orson/-/tree/master)

```bash
# Clone repository 
git clone https://gitlab.ifremer.fr/bioinfo/workflows/orson.git
```

You might at first run the pipeline with test dataset:

```bash
module load nextflow graphviz

# Get the config file for your cluster
wget https://raw.githubusercontent.com/nf-core/configs/master/conf/{{ platform.name|lower|replace(" ","_") }}.config -O orson/conf/{{ platform.name|lower|replace(" ","_") }}.config

# Run Orson with test dataset
cd orson/
srun nextflow run main.nf -profile test,singularity --downloadDB_enable false \
--hit_tool=diamond --blast_db "/path/to/indexed/db" \
-c conf/{{ platform.name|lower|replace(" ","_") }}.config -resume
```

The test dataset will be imported and then multiple analysis will be run:

* EggNOG-Mapper
* InterProScan
* BeeDeem
* Busco
* Diamond

Once you know it works well, you can run analysis with your own dataset:  

_Here an example of sbatch file :_  

```bash
#!/bin/bash
#SBATCH -p long

module load nextflow graphviz

nextflow run main.nf --fasta "query.fa" --query_type p -profile custom,singularity --downloadDB_enable false \
--blast_db "/path/to/indexed/db" -c conf/{{ platform.name|lower|replace(" ","_") }}.config -resume
```

By default, the previous tools will be launched, but it is possible to disable some of them.  

There are some useful arguments :  

**`--query_type [n,p]`**

Set to "n" for nucleic acid sequences input or to "p" for protein sequences.

**`--hit_tool [PLAST, BLAST, diamond]`**

Indicates the tool of your choice for the comparison of your sequences to the reference database.

**`--outdir`**

The output directory where the results will be published.

**`-w/--work-dir`**

The temporary directory where intermediate data will be written. (Can be `/scratch/` directory)  

Please refere as Orson's documentation for more details : [https://gitlab.ifremer.fr/bioinfo/workflows/orson/-/tree/master/docs](https://gitlab.ifremer.fr/bioinfo/workflows/orson/-/tree/master/docs)  

Note that Orson will check the presence of Singularity containers in `orson/container/` and if it doesn't find it, it will import them.

{% include "software/nf_orson_hectar.md" ignore missing %}

## Using nf-core

All nf-core pipelines have been successfully configured for use on the ABiMS cluster.
<!-- markdownlint-disable-next-line MD034 -->
Check this page: [github.com/nf-core/configs/blob/master/docs/{{ platform.name|lower|replace(" ","_") }}.md](https://github.com/nf-core/configs/blob/master/docs/{{ platform.name|lower|replace(" ","_") }}.md)
