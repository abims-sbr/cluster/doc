
☝️ How can I access to a Terminal 📺 in order to run the different commands?

- With [Open On Demand]({{ ondemand.url }}): [Shell Access](/cluster/doc/software/openondemand/getting_started/#clusters-shell-access)
- With a SSH Client: [Cluster/Log in](/cluster/doc/quick-start/#log-in)
