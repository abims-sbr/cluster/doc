
Several volumes of data storage are available on the {{ platform.name }} Cluster. Use it knowingly.

|                      | Usage                                   | Quota (default)  | Backup policy |
| -------------------- | --------------------------------------- | ----------------:| -------------:| {% for storage in storages %}
| **`{{ storage.path }}`** | {{ storage.usage }} | {{ storage.quota }} | {{ storage.backup }} |{% endfor %}

If you need or plan to use more storage space, please contact us: {{ platform.support_link }}
