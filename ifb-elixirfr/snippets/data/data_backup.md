## Backup

⚠️ There is no backup for the main storage.

### Snapshots

Some snapshots are available to protect against deletion by error but only one by day and for 5 days.
