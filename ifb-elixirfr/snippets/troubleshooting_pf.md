
### [GPU] How to know the availability of GPU nodes

We can use `sinfo` command with "Generic resources (gres)" information.

For example:

```bash
sinfo -N -O nodelist,partition:15,Gres:30,GresUsed:50 -p gpu
      NODELIST            PARTITION      GRES                          GRES_USED                                         
      gpu-node-01         gpu            gpu:1g.5gb:14                 gpu:1g.5gb:0(IDX:N/A)                             
      gpu-node-02         gpu            gpu:3g.20gb:2,gpu:7g.40gb:1   gpu:3g.20gb:1(IDX:0),gpu:7g.40gb:0(IDX:N/A)       
      gpu-node-03         gpu            gpu:7g.40gb:2                 gpu:7g.40gb:2(IDX:0-1)    
```

In other words:

- gpu-node-01: 14 profiles 1g.5gb, 0 used
- gpu-node-02: 2 profiles 3g.20gb, 1 used
- gpu-node-02: 1 profile 7g.40gb, 0 used
- gpu-node-03: 1 profile 7g.40gb, 2 used

So we can see which GPU/profiles are immediately available.

More information about this "profile" ("Multi-Instance GPU"):

- <https://ifb-elixirfr.gitlab.io/cluster/doc/slurm/slurm_at/#gpu-nodes>
- <https://docs.nvidia.com/datacenter/tesla/mig-user-guide/>

---
