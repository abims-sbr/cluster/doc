(Last update: 04/05/2022)

### GPU nodes

| Nbr | CPU | RAM (GB) | GPU | Type | Disk /tmp |
| --:| --:| --:| --:| --:| --:|
| 3 | 62 | 515 | 2 | [NVIDIA Ampere **A100** 40GB](https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/a100/pdf/nvidia-a100-datasheet-us-nvidia-1758950-r4-web.pdf) | 4 TB |

GPU Cards have been partitioned into isolated GPU instances with [Multi-Instance GPU (MIG)](https://docs.nvidia.com/datacenter/tesla/mig-user-guide/).

#### GPU Instance Profiles

⚠️ The values below can change. To check the current situation:

```bash
sinfo -Ne -p gpu --format "%.15N %.4c %.7m %G"
```

| Profile Name | GPU Memory  | GPU Compute Slice | Number of Instances Available |
| --:| --:| --:| --:|
| 1g.5gb | 5GB | 1 | 14 |
| 3g.20gb | 20GB | 3 | 2 |
| 7g.40gb | 40GB | 7 | 3 |
